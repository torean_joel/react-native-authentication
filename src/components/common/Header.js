import React from 'react';
import { View, Text } from 'react-native';

//create component
const Header = (props) => {
    return (
      <View style={styles.headerContainer}>
          <Text style={styles.headerTitle}>{ props.headerTitle }</Text>
      </View>
    );
}

const styles = {
    headerContainer: {
        backgroundColor: '#3b98ec',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        elevation: 2,
        position: 'relative',
    },
    headerTitle: {
        fontSize: 20,
        color: '#fff',
    }
};

//cant use deafult key if you want to export all from an index.js inside this directory
export { Header };