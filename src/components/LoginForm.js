import React from 'react';
import { Text } from 'react-native';
import Firebase from 'firebase';
//custom imports
import { Button, Card, CardSection, Input, Spinner } from './common';

export default class LoginForm extends React.Component {
  state = { 
    userEmail: '' ,
    userPassword: '',
    error: '',
    loading: false
  };

  render() {
    return (
      <Card>
        <CardSection>
          <Input 
            label='Email'
            placeholder='user@domain.com'
            value={ this.state.userEmail }
            onChangeText={ userEmail => this.setState({userEmail}) } />
        </CardSection>

        <CardSection>
          <Input 
              label='Password'
              placeholder='Password'
              secureTextEntry
              value={ this.state.userPassword }
              onChangeText={ userPassword => this.setState({userPassword}) } />
        </CardSection>

        <CardSection>
          { this.renderButton() }
        </CardSection>
        <Text style={ styles.errorTextStyle }>{ this.state.error }</Text>

      </Card>
      
    )
  }

  onSignIn() {
    this.setState({error: '', loading: true });

    Firebase.auth().signInWithEmailAndPassword(this.state.userEmail, this.state.userPassword)
      .then(() => { this.onLoginSuccess() })
      .catch(() => { this.onSignUp() })
  }

  onSignUp() {
    Firebase.auth().createUserWithEmailAndPassword(this.state.userEmail, this.state.userPassword)
      .then(() => { this.onLoginSuccess() })
      .catch(() => { this.onLoginFail() });
  }

  renderButton() {
    return (this.state.loading) ? <Spinner size='small'/> : <Button whenPressed={ () => this.onSignIn() }>Log in</Button>;
  }


  onLoginSuccess() {
    this.setState({
      userEmail: '',
      userPassword: '',
      loading: false,
      error: '',
    })
  }

  onLoginFail() {
    this.setState({
      error: 'Autnetication Failed',
      loading: false,
    })
  }
}

const styles = {
  errorTextStyle: {
    paddingTop: 10,
    paddingBottom: 10,
    alignSelf: 'center',
    fontSize: 15,
    color: 'red',
  }
}