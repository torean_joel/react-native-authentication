import React from 'react';
import { View, StatusBar } from 'react-native';
import Firebase from 'firebase';
//custom immports
import { Header, Button, Card, CardSection, Spinner } from './src/components/common';
import LoginForm from './src/components/LoginForm'

export default class App extends React.Component {
  state ={
    isLoggedIn: null
  }

  componentWillMount() {
    var config = {
      apiKey: 'AIzaSyABY--00Suj60hpMbrFg5zNVrc8uQ0ZWZs',
      authDomain: 'api-like-count.firebaseapp.com',
      databaseURL: 'https://api-like-count.firebaseio.com',
      projectId: 'api-like-count',
      storageBucket: 'api-like-count.appspot.com',
      messagingSenderId: '739555234091'
    };

    Firebase.initializeApp(config);
    //check the state of the user on the app login/logout status
    Firebase.auth().onAuthStateChanged((aUser) => {
      if (aUser) {
        this.setState({ isLoggedIn: true })
      }
      else {
        this.setState({ isLoggedIn: false })
      }
    });
  }

  onSignOut() {
    Firebase.auth().signOut();
  }

  renderLoginState() {
    switch (this.state.isLoggedIn) {
      case false:
        return <LoginForm />;
      case true:
        return (
          <Card>
            <CardSection>
              <Button whenPressed={() => this.onSignOut()}>Log Out</Button>
            </CardSection>
          </Card>
        )
      default:
        return (
          <Card>
            <CardSection>
              <Spinner size='small'/>
            </CardSection>
          </Card>
        )
    }
  }

  render() {
    return (
      <View>
        <View style={styles.StatusBar} />
        <Header headerTitle='Authentication' />
        { this.renderLoginState() }
      </View>
    );
  }
}

const styles = {
  StatusBar: {
    height: StatusBar.currentHeight
  }
};
